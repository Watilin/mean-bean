'use strict';

const EVENT_NAMESPACE = 'MeanBean';

// buffer to calculate an average value of milliseconds per frame (mspf)
const MSPF_BUFFER_SIZE = 96;

// Bean ////////////////////////////////////////////////////////////////

const Bean = (() => {
  const Bean = {};
  let lastId = -1;

  Bean.COLORS = [ 'red', 'yellow', 'green', 'purple', 'blue', 'black' ];

  // Bean bonds in the spritesheet are ordered as binary values
  // to get multiple bonds, just OR them together
  // e.g. { up, down, right } = 2 | 1 | 4 = 7
  Bean.BOND_NONE  = 0;
  Bean.BOND_DOWN  = 1;
  Bean.BOND_UP    = 2;
  Bean.BOND_RIGHT = 4;
  Bean.BOND_LEFT  = 8;

  Bean.STATE_STATIC     = Symbol('static');
  Bean.STATE_LEADING    = Symbol('leading');
  Bean.STATE_WOBBLING_H = Symbol('wobbling_h');
  Bean.STATE_WOBBLING_V = Symbol('wobbling_v');
  Bean.STATE_POPPING    = Symbol('popping');
  Bean.STATE_INVISIBLE  = Symbol('invisible');

  Bean.SPRITE_OFFSETS = {
    [Bean.STATE_STATIC]     : 0,
    [Bean.STATE_WOBBLING_H] : 16,
    [Bean.STATE_WOBBLING_V] : 17,
    [Bean.STATE_LEADING]    : 18,
    [Bean.STATE_POPPING]    : 20,
  };

  Bean.proto = {
    id: NaN,
    color: '',

    // logicalX/Y: integers, used as table indexes
    logicalX: NaN,
    logicalY: NaN,

    // offsets: pixel values, for move and rotation animations
    moveOffsetX: NaN,
    moveOffsetY: NaN,
    rotationOffsetX: NaN,
    rotationOffsetY: NaN,
    group: null,
    bonds: Bean.BOND_NONE,
    visualState: Bean.STATE_STATIC,
    isRemoved: false,
    player: null,

    moveTo: function moveTo(x, y) {
      this.logicalX = x;
      this.logicalY = y;
    },

    resetMoveOffset: function resetMoveOffset() {
      this.moveOffsetX = 0;
      this.moveOffsetY = 0;
    },

    addMoveOffset: function addRotationOffset(x, y) {
      this.moveOffsetX += x;
      this.moveOffsetY += y;
    },

    resetRotationOffset: function resetRotationOffset() {
      this.rotationOffsetX = 0;
      this.rotationOffsetY = 0;
    },

    addRotationOffset: function addRotationOffset(x, y) {
      this.rotationOffsetX += x;
      this.rotationOffsetY += y;
    },

    toString: function toString() {
      return 'Bean{#' + this.id + ' ' + this.color +
        (!isNaN(this.logicalX) ? (' ' + this.logicalX + ',' + this.logicalY) : '') +
        (this.isRemoved ? '*}' : '}');
    }
  };

  Bean.create = function createBean(color) {
    const bean = Object.create(Bean.proto);

    bean.id = ++lastId;

    color = color || Bean.COLORS[0];
    bean.color = color;

    bean.resetMoveOffset();
    bean.resetRotationOffset();

    // console.log('bean created: %s', bean);

    return bean;
  };

  return Bean;
})();

// Pair ////////////////////////////////////////////////////////////////

const Pair = (() => {
  const Pair = {};

  Pair.proto = {
    beanA: null,
    beanB: null,
    orientation: null, // from beanA to beanB
  };

  Pair.create = function createPair(beanA, beanB) {
    const pair = Object.create(Pair.proto);
    // console.log('pair created (%s, %s)', beanA, beanB);

    pair.beanA = beanA;
    pair.beanB = beanB;

    return pair;
  };

  return Pair;
})();

// Group ///////////////////////////////////////////////////////////////

const Group = (() => {
  const Group = {};
  let lastId = -1;

  Group.proto = {
    beans: null,
    id: NaN,
    color: '',
    toBeRemoved: false,

    checkConsistency: function checkConsistency() {
      const thisGroup = this;
      if (!this.beans.every((bean) => bean.group === thisGroup)) {
        throw new Error(`inconsistent group ${this}`);
      }
    },

    getLength: function getLength() {
      this.checkConsistency();
      return this.beans.length;
    },

    addBean: function addBean(bean) {
      if (this.toBeRemoved) {
        throw new Error('can’t reuse a group that has been emptied: ' + this);
      }
      if (bean.group) bean.group.removeBean(bean);
      if (!this.containsBean(bean)) this.beans.push(bean);
      bean.group = this;
      this.checkConsistency();
      this.updateBonds();
    },

    removeBean: function removeBean(bean) {
      this.checkConsistency();
      if (!this.containsBean(bean)) {
        throw new Error(`bean ${bean} not in group ${this}`);
      }
      this.beans.splice(this.beans.indexOf(bean), 1);
      bean.group = null;
      bean.bonds = Bean.BOND_NONE;
      if (this.beans.length) {
        this.updateBonds();
      }
      else {
        this.toBeRemoved = true;
        // console.log('group %s empty, won’t be used anymore', this);
      }
    },

    forEachBean: function forEachBean(func) {
      // need to iterate over a copy because beans may be removed during
      // the iteration
      this.beans.slice().forEach(func);
    },

    containsBean: function containsBean(bean) {
      return this.beans.includes(bean);
    },

    updateBonds: function updateBonds() {
      const list = this.beans.slice();
      for (const bean of list) {
        bean.bonds = Bean.BOND_NONE;
      }

      while (list.length > 1) {
        const beanA = list.pop();
        const ax = beanA.logicalX;
        const ay = beanA.logicalY;
        for (const beanB of list) {
          const bx = beanB.logicalX;
          const by = beanB.logicalY;
          if (ax === bx) {
            if (+1 === ay - by) {
              beanA.bonds |= Bean.BOND_UP;
              beanB.bonds |= Bean.BOND_DOWN;
            }
            else if (-1 === ay - by) {
              beanA.bonds |= Bean.BOND_DOWN;
              beanB.bonds |= Bean.BOND_UP;
            }
          }
          else if (ay === by) {
            if (+1 === ax - bx) {
              beanA.bonds |= Bean.BOND_LEFT;
              beanB.bonds |= Bean.BOND_RIGHT;
            }
            else if (-1 === ax - bx) {
              beanA.bonds |= Bean.BOND_RIGHT;
              beanB.bonds |= Bean.BOND_LEFT;
            }
          }
        }
      }
    },

    toString: function toString() {
      return `Group{#${this.id} ${this.color} [${this.beans.length}]}` +
        (this.beans.length ?
          (` [ ${ this.beans.map((bean) => `#${bean.id}`).join(', ') } ]`) :
          '');
    }
  };

  Group.create = function createGroup(...beans) {
    const group = Object.create(Group.proto);

    if (!beans.length) {
      throw new Error('you may not create an empty group');
    }
    group.id = ++lastId;

    group.beans = [];
    group.color = beans[0].color;
    for (const bean of beans) {
      if (bean.color !== group.color) {
        throw new Error('bean color doesn’t match group’s');
      }
      group.addBean(bean);
    }

    console.log('group created: %s', group);
    return group;
  };

  return Group;
})();

// GameLogic ///////////////////////////////////////////////////////////

const GameLogic = (() => {
  const GameLogic = {};

  GameLogic.GRID_HEIGHT = 12;
  GameLogic.GRID_WIDTH  = 6;

  GameLogic.DROP_X = Math.floor((GameLogic.GRID_WIDTH - 1) / 2);
  GameLogic.DROP_Y = 0;

  GameLogic.PLAYER   = Symbol('player');
  GameLogic.OPPONENT = Symbol('opponent');

  GameLogic.STATE_UNINITIALIZED  = Symbol('uninitialized');
  GameLogic.STATE_INTERACTIVE    = Symbol('interactive');
  GameLogic.STATE_RESOLVING      = Symbol('resolving');
  GameLogic.STATE_GAME_OVER      = Symbol('game over');
  GameLogic.STATE_PAUSED         = Symbol('paused');

  GameLogic.LEFT   = Symbol('left');
  GameLogic.RIGHT  = Symbol('right');
  GameLogic.UP     = Symbol('up');
  GameLogic.DOWN   = Symbol('down');
  GameLogic.CW     = Symbol('clockwise');
  GameLogic.CCW    = Symbol('counterclockwise');

  GameLogic.ACTION_MOVE   = Symbol('move');
  GameLogic.ACTION_ROTATE = Symbol('rotate');

  GameLogic.AUTO_FALL_DELAYS = { '1': 32, '2': 20, '3': 16, '4': 8, '5': 4 };

  GameLogic.ROTATION_TABLE = {
    [GameLogic.UP]: {
      [GameLogic.CCW] : { x: -1, y: +1, orientation: GameLogic.LEFT  },
      [GameLogic.CW]  : { x: +1, y: +1, orientation: GameLogic.RIGHT },
    },
    [GameLogic.LEFT]: {
      [GameLogic.CCW] : { x: +1, y: +1, orientation: GameLogic.DOWN  },
      [GameLogic.CW]  : { x: +1, y: -1, orientation: GameLogic.UP    },
    },
    [GameLogic.DOWN]: {
      [GameLogic.CCW] : { x: +1, y: -1, orientation: GameLogic.RIGHT },
      [GameLogic.CW]  : { x: -1, y: -1, orientation: GameLogic.LEFT  },
    },
    [GameLogic.RIGHT]: {
      [GameLogic.CCW] : { x: -1, y: -1, orientation: GameLogic.UP    },
      [GameLogic.CW]  : { x: -1, y: +1, orientation: GameLogic.DOWN  },
    },
  };

  GameLogic.GRACE_FRAMES = 32;

  GameLogic.proto = {
    currentPair: null,
    nextPair: null,
    beans: null,

    autoFallDelay: NaN, // in frames
    lastAutoFallTime: -Infinity,

    remainingGraceFrames: NaN,
    graceCountingDown: false,

    display: null,
    audioPlayer: null,
    server: null,
    clock: null,
    score: null,
    inputHandler: null,

    animations: null,

    state: GameLogic.STATE_UNINITIALIZED,

    init: function init() {
      this.state = GameLogic.STATE_INTERACTIVE;
      this.updateSpeed();
      this.drop();
    },

    updateSpeed: function updateSpeed() {
      const dif = this.server.getDifficulty();
      this.autoFallDelay = GameLogic.AUTO_FALL_DELAYS[dif];
    },

    tick: function tick(time) {
      if (GameLogic.STATE_INTERACTIVE === this.state) {
        this.handleCommands(this.inputHandler.readCommands(time));
        if (this.graceCountingDown) {
          this.remainingGraceFrames--;
          if (this.remainingGraceFrames <= 0) {
            this.pushDown(this.currentPair);
          }
        }
        else if (time - this.lastAutoFallTime >= this.autoFallDelay * Clock.ONE_FRAME) {
          this.move(GameLogic.DOWN, { auto: true });
          this.lastAutoFallTime = time;
        }
      }
    },

    resetGraceCountdown: function resetGraceFrames() {
      this.remainingGraceFrames = GameLogic.GRACE_FRAMES;
      this.graceCountingDown = false;
    },

    startGraceCountdown: function startGraceCountdown() {
      this.graceCountingDown = true;
    },

    stopGraceCountdown: function stopGraceCountdown() {
      this.graceCountingDown = false;
    },

    drop: function drop() {
      if (this.beans[GameLogic.DROP_X][GameLogic.DROP_Y]) {
        this.gameOver();
        return;
      }
      for (let i = 0; i < GameLogic.GRID_WIDTH; i++) {
        if (this.beans[i][-2]) {
          this.gameOver();
          return;
        }
      }

      const pair = this.getNextPair();
      console.log('=== new round: %s %s ===', pair.beanA, pair.beanB);

      const beanA = pair.beanA;
      const beanB = pair.beanB;

      beanA.moveTo(GameLogic.DROP_X, GameLogic.DROP_Y - 1);
      beanB.moveTo(GameLogic.DROP_X, GameLogic.DROP_Y - 2);
      beanA.resetMoveOffset();
      beanB.resetMoveOffset();
      pair.orientation = GameLogic.UP;
      this.currentPair = pair;

      this.animations.leadingBeanBlinking = this.display.startAnimation({
        type: 'loop',
        period: 20,
        steps: {
          0: () => { beanA.visualState = Bean.STATE_LEADING },
          11: () => { beanA.visualState = Bean.STATE_STATIC },
        },
      });

      this.resetGraceCountdown();
    },

    getNextPair: function getNextPair() {
      if (!this.nextPair) {
        this.nextPair = this.server.requestPair();
        this.setPreviewPair(this.nextPair);
      }

      const pair = this.nextPair;
      this.nextPair = this.server.requestPair();
      this.setPreviewPair(this.nextPair);
      return pair;
    },

    setPreviewPair: function setPreviewPair(pair) {
      if (GameLogic.PLAYER === pair.beanA.player) {
        pair.beanA.moveTo(7, 1);
        pair.beanB.moveTo(7, 2);
        pair.beanA.addMoveOffset(0, Display.GRID_PITCH / 2);
        pair.beanB.addMoveOffset(0, Display.GRID_PITCH / 2);
      }
      else {
        pair.beanA.moveTo(-2, 1);
        pair.beanB.moveTo(-2, 2);
        pair.beanA.addMoveOffset(0, Display.GRID_PITCH / 2);
        pair.beanB.addMoveOffset(0, Display.GRID_PITCH / 2);
      }
      this.display.registerBean(pair.beanA);
      this.display.registerBean(pair.beanB);
    },

    handleCommands: function handleCommands(commands) {
      if (GameLogic.STATE_INTERACTIVE === this.state) {
        for (const command of commands) {
          switch (command.action) {
            case GameLogic.ACTION_MOVE: {
              this.move(command.argument);
              break;
            }
            case GameLogic.ACTION_ROTATE: {
              this.rotate(command.argument);
              break;
            }
            default: {
              throw new Error(`Unhandled command action: ${command.action}`);
            }
          }
        }
      }
    },

    hasRoomBelow: function hasRoomBelow(obj) {
      if (Pair.proto.isPrototypeOf(obj)) {
        switch (obj.orientation) {
          case GameLogic.UP: {
            return this.hasRoomBelow(obj.beanA);
          }
          case GameLogic.DOWN: {
            return this.hasRoomBelow(obj.beanB);
          }
          default: {
            return this.hasRoomBelow(obj.beanA) && this.hasRoomBelow(obj.beanB);
          }
        }
      }

      else if (Bean.proto.isPrototypeOf(obj)) {
        const x = obj.logicalX;
        const y = obj.logicalY;
        if (y >= GameLogic.GRID_HEIGHT - 1) return false;
        if (this.beans[x][y + 1]) return false;
        return true;
      }

      else {
        throw new Error(`Unhandled object ${obj}`);
      }
    },

    move: function move(dir, extraArgs = {}) {
      const pair = this.currentPair;
      const ori = pair.orientation;
      const a = pair.beanA;
      const b = pair.beanB;

      let ax = a.logicalX;
      let bx = b.logicalX;
      let ay = a.logicalY;
      let by = b.logicalY;

      const previousAx = ax;
      const previousBx = bx;
      const previousAy = ay;
      const previousBy = by;

      this.beans[ax][ay] = null;
      this.beans[bx][by] = null;

      let isBottom = false;

      switch (dir) {
        case GameLogic.LEFT:
          if (ax > 0 && bx > 0 && (
              (GameLogic.LEFT === ori && !this.beans[bx - 1][by]) ||
              (GameLogic.RIGHT === ori && !this.beans[ax - 1][ay]) ||
              (!this.beans[ax - 1][ay] && !this.beans[bx - 1][by])
          )) {
            ax = ax - 1;
            bx = bx - 1;
          }
          break;

        case GameLogic.RIGHT:
          if (ax < GameLogic.GRID_WIDTH - 1 && bx < GameLogic.GRID_WIDTH - 1 && (
              (GameLogic.LEFT === ori && !this.beans[ax + 1][ay]) ||
              (GameLogic.RIGHT === ori && !this.beans[bx + 1][by]) ||
              (!this.beans[ax + 1][ay] && !this.beans[bx + 1][by])
          )) {
            ax = ax + 1;
            bx = bx + 1;
          }
          break;

        case GameLogic.DOWN:
          if (ay < GameLogic.GRID_HEIGHT - 1 && by < GameLogic.GRID_HEIGHT - 1 && (
              (GameLogic.UP === ori && !this.beans[ax][ay + 1]) ||
              (GameLogic.DOWN === ori && !this.beans[bx][by + 1]) ||
              (!this.beans[ax][ay + 1] && !this.beans[bx][by + 1])
          )) {
            ay = ay + 1;
            by = by + 1;
          }
          else {
            isBottom = true;
          }
          break;
      }

      a.moveTo(ax, ay);
      b.moveTo(bx, by);
      this.beans[ax][ay] = a;
      this.beans[bx][by] = b;

      if (isBottom) {
        if (extraArgs && extraArgs.auto) {
          this.startGraceCountdown();
        }
        else { // manual push down, player wants to go fast
          this.pushDown(pair);
        }
      }
      else {
        this.stopGraceCountdown();
      }

      const resetOffsets = () => {
        a.resetMoveOffset();
        b.resetMoveOffset();
      };

      if (ax !== previousAx) {
        this.stopGraceCountdown();
        a.addMoveOffset((previousAx - ax) * Display.GRID_PITCH / 2, 0);
        b.addMoveOffset((previousBx - bx) * Display.GRID_PITCH / 2, 0);
        this.audioPlayer.playClip('move');
        const task = this.clock.addTask({
          mode: Task.MODE_TIMEOUT,
          frames: 1,
          callback: resetOffsets,
        });
      }
      else if (ay !== previousAy) {
        a.addMoveOffset(0, -Display.GRID_PITCH / 2);
        b.addMoveOffset(0, -Display.GRID_PITCH / 2);
        const frames = (extraArgs && extraArgs.auto) ?
          (this.autoFallDelay / 2) : 1;
        this.clock.addTask({
          mode: Task.MODE_TIMEOUT,
          frames: frames,
          callback: resetOffsets,
        });
      }
    },

    rotate: function rotate(dir) {
      const pair = this.currentPair;
      const a = pair.beanA;
      const b = pair.beanB;

      const ax = a.logicalX;
      const bx = b.logicalX;
      const ay = a.logicalY;
      const by = b.logicalY;

      this.beans[ax][ay] = null;
      this.beans[bx][by] = null;

      const coordChange = GameLogic.ROTATION_TABLE[pair.orientation][dir];

      let newBx = bx + coordChange.x;
      let newBy = by + coordChange.y;

      let hasChanged = false;

      if (newBx >= 0 && newBx < GameLogic.GRID_WIDTH &&
          newBy < GameLogic.GRID_HEIGHT && !this.beans[newBx][newBy]) {
        // normal case
        b.logicalX = newBx;
        b.logicalY = newBy;
        pair.orientation = coordChange.orientation;
        hasChanged = true;
      }

      else {
        // kick case
        const kickShift = {};
        if (GameLogic.DOWN === coordChange.orientation ||
            GameLogic.UP === coordChange.orientation) {
          // floor kick
          kickShift.x = 0;
          kickShift.y = -coordChange.y;
        }
        else {
          // wall kick
          kickShift.x = -coordChange.x;
          kickShift.y = 0;
        }
        newBx = ax + kickShift.x;
        newBy = ay + kickShift.y;

        if (newBx >= 0 && newBx < GameLogic.GRID_WIDTH &&
            newBy < GameLogic.GRID_HEIGHT && !this.beans[newBx][newBy]) {
          b.logicalX = ax;
          b.logicalY = ay;
          a.logicalX = newBx;
          a.logicalY = newBy;
          pair.orientation = coordChange.orientation;
          hasChanged = true;
        }
      }

      a.resetRotationOffset();
      b.resetRotationOffset();
      this.beans[a.logicalX][a.logicalY] = a;
      this.beans[b.logicalX][b.logicalY] = b;

      if (hasChanged) {
        this.stopGraceCountdown();
        this.audioPlayer.playClip('rotate');

        if (this.animations.rotation) {
          this.animations.rotation.stop();
        }
        this.animations.rotation = this.display.startAnimation({
          type: 'rotation',
          bean: b,
          signX: coordChange.x,
          signY: coordChange.y,
          rotDir: dir,
        });
      }
    },

    pushDown: function pushDown(pair) {
      const a = pair.beanA;
      const b = pair.beanB;

      if (this.animations.rotation) {
        this.animations.rotation.stop();
        a.resetRotationOffset();
        b.resetRotationOffset();
      }
      a.resetMoveOffset();
      b.resetMoveOffset();

      const ax = a.logicalX;
      const bx = b.logicalX;
      const ay = a.logicalY;
      const by = b.logicalY;

      if (ay < GameLogic.GRID_HEIGHT - 1 && !this.beans[ax][ay + 1]) {
        this.letFall(a);
      }
      if (by < GameLogic.GRID_HEIGHT - 1 && !this.beans[bx][by + 1]) {
        this.letFall(b);
      }

      this.audioPlayer.playClip('land');

      this.state = GameLogic.STATE_RESOLVING;
      this.clock.addTask({
        mode: Task.MODE_TIMEOUT,
        frames: 20,
        callback: (time) => { this.resolve(); },
      });
    },

    letFall: function letFall(bean) {
      // console.log('letting fall %s (at least)', bean);
      const x = bean.logicalX;

      let y = bean.logicalY;
      let oldY = y;
      while (y < GameLogic.GRID_HEIGHT - 1 && !this.beans[x][y + 1]) y++;
      const fallHeight = y - oldY;

      let beanToFall = bean;
      do {
        if (beanToFall.group) beanToFall.group.removeBean(beanToFall);
        oldY = beanToFall.logicalY;
        this.beans[x][oldY] = null;
        beanToFall.moveTo(x, oldY + fallHeight);
        beanToFall.resetMoveOffset();
        this.beans[x][beanToFall.logicalY] = beanToFall;
        beanToFall = this.beans[x][oldY - 1];
      } while (beanToFall);
    },

    resolve: function resolve(chainLevel = 0) {
      this.animations.leadingBeanBlinking.stop();
      this.currentPair.beanA.visualState = Bean.STATE_STATIC;
      this.dump();

      const allGroups = [];
      const groupsToRemove = [];
      const hoveringBeans = [];
      const logic = this;
      const clock = logic.clock;

      let i, bean, group, adjacentBeans, lastGroup;
      let j = GameLogic.GRID_HEIGHT;

      // iterating backwards
      while (j--) for (i = GameLogic.GRID_WIDTH; i--; ) {
        bean = this.beans[i][j];
        if (bean) {
          adjacentBeans = [];
          // looking only left and top beans since iteration is backwards
          if (i > 0) adjacentBeans.push(this.beans[i - 1][j]);
          if (j > 0) adjacentBeans.push(this.beans[i][j - 1]);

          for (const adj of adjacentBeans) {
            if (adj && adj.color === bean.color) {
              if (adj.group && bean.group) {
                if (adj.group !== bean.group) {
                  bean.group.forEachBean((otherBean) => {
                    adj.group.addBean(otherBean);
                  });
                }
                group = adj.group;
              }
              else if (adj.group) {
                adj.group.addBean(bean);
                group = adj.group;
              }
              else if (bean.group) {
                bean.group.addBean(adj);
                group = bean.group;
              }
              else {
                group = Group.create(bean, adj);
              }
            }
          }

          if (group && !allGroups.includes(group)) allGroups.push(group);
        }
      }

      for (const group of allGroups) {
        if (group.getLength() >= 4) {
          groupsToRemove.push(group);
        }
      }

      if (groupsToRemove.length) {
        const setStateInvisible = (bean) => bean.visualState = Bean.STATE_INVISIBLE;
        const setStateStatic    = (bean) => bean.visualState = Bean.STATE_STATIC;
        const setStatePopping   = (bean) => bean.visualState = Bean.STATE_POPPING;

        for (let n = 5; n < 27; n += 2) {
          clock.addTask({
            mode: Task.MODE_TIMEOUT,
            frames: n,
            callback: (time) => {
              for (const group of groupsToRemove) {
                group.forEachBean(setStateInvisible);
              }
            },
          });

          clock.addTask({
            mode: Task.MODE_TIMEOUT,
            frames: n + 1,
            callback: (time) => {
              for (const group of groupsToRemove) {
                group.forEachBean(setStateStatic);
              }
            },
          });
        }

        clock.addTask({
          mode: Task.MODE_TIMEOUT,
          frames: 29,
          callback: (time) => {
            this.audioPlayer.playClip('chain' + chainLevel);
            for (const group of groupsToRemove) {
              group.forEachBean(setStatePopping);
            }
          },
        });

        clock.addTask({
          mode: Task.MODE_TIMEOUT,
          frames: 60,
          callback: (time) => {
            console.log('- groups will be removed now');
            const hoveringBeans = [];

            for (const group of groupsToRemove) {
              logic.removeGroup(group);
              group.forEachBean((bean) => {
                if (bean.logicalY > 0) {
                  const beanOnTop = logic.beans[bean.logicalX][bean.logicalY - 1];
                  if (beanOnTop &&
                      beanOnTop.group !== bean.group &&
                      !beanOnTop.isRemoved) {
                    hoveringBeans.push(beanOnTop);
                  }
                }
              });
            }

            if (hoveringBeans.length) {
              clock.addTask({
                mode: Task.MODE_TIMEOUT,
                frames: 21, // TODO check again after particle animation is done
                callback: (time) => {
                  console.log('-- beans will fall now');
                  for (const bean of hoveringBeans) {
                    if (!bean.isRemoved) logic.letFall(bean);
                  }
                  logic.resolve(chainLevel + 1);
                },
              });
            }
            else {
              console.log('-- no hovering beans, end');
              logic.state = GameLogic.STATE_INTERACTIVE;
              logic.drop();
            }
          },
        });
      }
      else {
        // console.log('- no groups to remove, end');
        logic.state = GameLogic.STATE_INTERACTIVE;
        logic.drop();
      }
    },

    removeGroup: function removeGroup(group) {
      console.log('removing group %s', group);
      const logic = this;
      group.forEachBean((bean) => {
        logic.removeBean(bean);
      });
    },

    removeBean: function removeBean(bean) {
      if (bean.isRemoved) throw new Error('bean already removed ' + bean);
      // console.log('removing bean %s', bean);
      this.beans[bean.logicalX][bean.logicalY] = null;
      bean.isRemoved = true;
      this.display.unregisterBean(bean);
    },

    dump: function dump() {
      let output = '    0 1 2 3 4 5\n';
      let styles = [];
      let buffer;
      let stylesBuffer;
      let isLinePopulated;
      for (let j = 0; j < GameLogic.GRID_HEIGHT; j++) {
        buffer = (j < 10 ? ' ' : '') + j + ' ';
        stylesBuffer = [];
        isLinePopulated = false;

        for (let i = 0; i < GameLogic.GRID_WIDTH; i++) {
          const bean = this.beans[i][j];
          let symbol = '';
          if (bean) {
            isLinePopulated = true;
            symbol = '%c' + bean.color.charAt(0).toUpperCase() + '%c';
            stylesBuffer.push('color: ' + bean.color, 'color: default');
          }
          else {
            symbol = '.';
          }
          buffer += ' ' + symbol;
        }

        if (isLinePopulated) {
          output += buffer + '\n';
          styles = styles.concat(stylesBuffer);
        }
      }
      console.log.apply(console, [ output ].concat(styles));
    },

    gameOver: function gameOver() {
      console.log('======= Game Over =======');
      this.state = GameLogic.STATE_GAME_OVER;
      for (let i = GameLogic.GRID_WIDTH; i--; ) {
        for (let j = GameLogic.GRID_HEIGHT; j--; ) {
          const bean = this.beans[i][j];
          // if (bean) this.removeBean(bean);
        }
      }
    }
  };

  GameLogic.create = function createGameLogic() {
    const gameLogic = Object.create(GameLogic.proto);
    console.log('gameLogic created');

    const beans = [];
    for (let i = GameLogic.GRID_WIDTH; i--; ) {
      beans[i] = [];
      for (let j = GameLogic.GRID_HEIGHT; j--; ) {
        beans[i][j] = null;
      }
    }
    gameLogic.beans = beans;

    gameLogic.animations = {};

    return gameLogic;
  };

  return GameLogic;
})();

// Score ///////////////////////////////////////////////////////////////

const Score = (() => {
  const Score = {};

  Score.CHAIN_POWER_TABLE = [ 0, 8, 16, 32, 64, 128, 256, 512, 999 ];

  Score.proto = {
    chain: NaN,
    chainPower: NaN,

    upgradeChain: function upgradeChain() {
      this.chain = this.chain + 1 || 0;
      const powerIndex =
        Math.min(this.chain, Score.CHAIN_POWER_TABLE.length - 1);
      this.chainPower = Score.CHAIN_POWER_TABLE[powerIndex];
      console.log('chain step = %d -> power = %d',
        this.chain, this.chainPower);
    }
  };

  Score.create = function createScore() {
    const score = Object.create(Score.proto);
    console.log('score created');

    score.upgradeChain();
    return score;
  };

  return Score;
})();

// Arena ///////////////////////////////////////////////////////////////

const Arena = (() => {
  const Arena = {};

  Arena.RATIO = 960 / 674;

  Arena.proto = {};

  Arena.create = function createArena($container) {
    const arena = Object.create(Arena.proto);
    console.log('arena created (#' + $container.id + ')');

    const server       = Server.create();
    const clock        = Clock.create();
    const display      = Display.create();
    const audioPlayer  = AudioPlayer.create();
    const assetManager = AssetManager.create();
    const inputHandler = InputHandler.create();

    clock.server = server;
    display.clock = clock;

    display.assetManager     = assetManager;
    audioPlayer.assetManager = assetManager;

    const acx = new AudioContext();
    assetManager.audioContext = acx;
    audioPlayer.audioContext = acx;

    document.addEventListener(AssetManager.ALL_ASSETS_LOADED_EVENT,
      function listener(event) {
        document.removeEventListener(
          AssetManager.ALL_ASSETS_LOADED_EVENT, listener);

        const gameLogic = GameLogic.create();
        gameLogic.display      = display;
        gameLogic.audioPlayer  = audioPlayer;
        gameLogic.server       = server;
        gameLogic.clock        = clock;
        gameLogic.inputHandler = inputHandler;

        display.setContainer($container);

        const mspfBuffer = new Uint16Array(MSPF_BUFFER_SIZE);
        mspfBuffer.fill(0);
        const averageMspf = () =>
          mspfBuffer.reduce((a, b) => a + b) / MSPF_BUFFER_SIZE;
        let bufferIndex = 0;
        let previousTime = -Infinity;

        clock.start((time) => {
          gameLogic.init();

          clock.addTask({
            mode: Task.MODE_PER_FRAME,
            callback: (time) => {
              if (previousTime > 0) {
                mspfBuffer[bufferIndex] = time - previousTime;
              }
              previousTime = time;
              bufferIndex++;
              if (bufferIndex >= MSPF_BUFFER_SIZE) {
                bufferIndex = 0;
              }

              gameLogic.tick(time);
              display.drawFrame(time);
              display.drawMspf(averageMspf());
            },
          });
        });
      });

    return arena;
  };

  return Arena;
})();

// InputHandler ////////////////////////////////////////////////////////

const InputHandler = (() => {
  const InputHandler = {};

  InputHandler.COOLDOWNS = { // in frames
    'MOVE_SIDE_INITIAL': 8,
    'MOVE_SIDE_REPEAT': 3,
    'MOVE_DOWN': 2,
  };

  InputHandler.COOLDOWN_STATE_INITIAL   = Symbol('initial');
  InputHandler.COOLDOWN_STATE_REPEATING = Symbol('repeating');

  InputHandler.DEFAULT_BINDINGS = {
    81 : { action: GameLogic.ACTION_MOVE,   argument: GameLogic.LEFT  }, // Q
    83 : { action: GameLogic.ACTION_MOVE,   argument: GameLogic.DOWN  }, // S
    68 : { action: GameLogic.ACTION_MOVE,   argument: GameLogic.RIGHT }, // D
    37 : { action: GameLogic.ACTION_ROTATE, argument: GameLogic.CCW   }, // left arrow
    39 : { action: GameLogic.ACTION_ROTATE, argument: GameLogic.CW    }, // right arrow
  };
  InputHandler.DEFAULT_BINDINGS[65] = InputHandler.DEFAULT_BINDINGS[81]; // A (Bépo)
  InputHandler.DEFAULT_BINDINGS[85] = InputHandler.DEFAULT_BINDINGS[83]; // U (Bépo)
  InputHandler.DEFAULT_BINDINGS[73] = InputHandler.DEFAULT_BINDINGS[68]; // I (Bépo)

  InputHandler.proto = {
    bindings: null,
    keysDown: null,

    // cooldown = {
    //   action
    //     GameLogic.ACTION_{MOVE|ROTATE}
    //   argument
    //     GameLogic.{LEFT|RIGHT|DOWN|CCW|CW}
    //   lastTime
    //     a time as passed to our requestAnimationFrame callback
    //   state
    //     InputHandler.COMMAND_STATE_{INITIAL|REPEATING}
    // }
    ongoingCooldowns: null,

    onKeydown: function onKeydown(event) {
      // ignore IME composition events
      // https://developer.mozilla.org/en-US/docs/Web/API/Element/keydown_event
      if (event.isComposing || event.keyCode === 229) return;

      if (!(event.altKey || event.ctrlKey || event.metaKey)) {
        const keyCode = event.keyCode;
        if (keyCode in this.bindings) {
          if (!this.keysDown.includes(keyCode)) {
            this.keysDown.push(keyCode);
          }
          event.preventDefault();
        }
      }
    },

    onKeyup: function onKeyup(event) {
      const keyCode = event.keyCode;
      if (this.keysDown.includes(keyCode)) {
        this.keysDown.splice(this.keysDown.indexOf(keyCode), 1);
      }
    },

    // Process hardware inputs, resolves conflicts and cooldowns,
    // and send them to GameLogic in the form of commands.
    readCommands: function readCommands(time) {
      const cleanInputs = [];
      let moveSideRequested = false;
      let moveDownRequested = false;
      let rotateRequested = false;

      // in case of conflicting inputs, take the most recent one
      for (let i = this.keysDown.length; i--; ) {
        const input = this.bindings[ this.keysDown[i] ];

        switch (input.action) {
          case GameLogic.ACTION_MOVE: {
            switch (input.argument) {
              case GameLogic.LEFT:
              case GameLogic.RIGHT: {
                if (!moveSideRequested) {
                  moveSideRequested = true;
                  cleanInputs.push(input);
                }
                break;
              }
              case GameLogic.DOWN: {
                if (!moveDownRequested) {
                  moveDownRequested = true;
                  cleanInputs.push(input);
                }
                break;
              }
              default: {
                throw new Error(`Invalid input argument ${input.argument}` +
                  ` for action ${input.action}`);
              }
            }
            break;
          }
          case GameLogic.ACTION_ROTATE: {
            if (!rotateRequested) {
              rotateRequested = true;
              cleanInputs.push(input);
            }
            break;
          }
          default: {
            throw new Error('Unknown or missing input action');
          }
        }
      }
      // respect input order
      cleanInputs.reverse();

      // transform inputs into commands to be used by GameLogic
      const commands = [];
      const nextCooldowns = [];
      cleanInputs.forEach((input) => {
        // check against ongoing commands
        let matched = false;
        for (const ongoing of this.ongoingCooldowns) {
          if (input.action === ongoing.action &&
            input.argument === ongoing.argument
          ) {
            switch (input.action) {
              case GameLogic.ACTION_MOVE: {
                switch (input.argument) {
                  case GameLogic.LEFT:
                  case GameLogic.RIGHT: {
                    const delay = (ongoing.state ===
                      InputHandler.COOLDOWN_STATE_INITIAL) ?
                        InputHandler.COOLDOWNS.MOVE_SIDE_INITIAL :
                        InputHandler.COOLDOWNS.MOVE_SIDE_REPEAT;
                    if (time - ongoing.lastTime >= delay * Clock.ONE_FRAME) {
                      const newCommand = ongoing;
                      newCommand.lastTime = time;
                      newCommand.state = InputHandler.COOLDOWN_STATE_REPEATING;
                      commands.push(newCommand);
                      nextCooldowns.push(newCommand);
                    }
                    else {
                      nextCooldowns.push(ongoing);
                    }
                    break;
                  }
                  case GameLogic.DOWN: {
                    if (time - ongoing.lastTime >=
                      InputHandler.COOLDOWNS.MOVE_DOWN * Clock.ONE_FRAME
                    ) {
                      const newCommand = ongoing;
                      newCommand.lastTime = time;
                      newCommand.state = InputHandler.COOLDOWN_STATE_REPEATING;
                      commands.push(newCommand);
                      nextCooldowns.push(newCommand);
                    }
                    else {
                      nextCooldowns.push(ongoing);
                    }
                    break;
                  }
                }
                break;
              }
              case GameLogic.ACTION_ROTATE: {
                // for rotate commands, no repetition. This can be seen as
                // infinite cooldown until the player releases the input.
                nextCooldowns.push(ongoing);
                break;
              }
            }
            matched = true;
            break;
          }
        }
        if (!matched) {
          const newCommand = {
            action: input.action,
            argument: input.argument,
            lastTime: time,
            state: InputHandler.COOLDOWN_STATE_INITIAL,
          };
          commands.push(newCommand);
          nextCooldowns.push(newCommand);
        }
      });

      // Ongoing cooldowns that didn’t match any input are not relevant
      // anymore, discard them
      this.ongoingCooldowns = nextCooldowns;

      return commands;
    },
  };

  InputHandler.create = function createInputHandler() {
    const inputHandler = Object.create(InputHandler.proto);
    console.log('inputHandler created');

    inputHandler.bindings = InputHandler.DEFAULT_BINDINGS;
    inputHandler.keysDown = [];
    inputHandler.ongoingCooldowns = [];

    document.addEventListener('keydown', function (event) {
      // console.log(`DOM keydown: ${event.keyCode}`);
      inputHandler.onKeydown(event);
    });

    document.addEventListener('keyup', function (event) {
      // console.log(`DOM keyup: ${event.keyCode}`);
      inputHandler.onKeyup(event);
    });

    return inputHandler;
  };

  return InputHandler;
})();

// Server //////////////////////////////////////////////////////////////

const Server = (() => {
  const Server = {};

  Server.proto = {
    difficulty: NaN,

    getDifficulty: function getDifficulty() {
      return this.difficulty;
    },

    setDifficulty: function setDifficulty(difficulty) {
      this.difficulty = Math.min(difficulty, 5);
    },

    requestPair: function requestPair() {
      const dif = this.difficulty;
      if (Number.isNaN(dif)) {
        throw new Error('difficulty not set at server side');
      }

      const n = (dif > 1) ? 5 : 4;
      const beanA = Bean.create(Bean.COLORS[Math.floor(n * Math.random())]);
      beanA.player = GameLogic.PLAYER;
      const beanB = Bean.create(Bean.COLORS[Math.floor(n * Math.random())]);
      beanB.player = GameLogic.PLAYER;
      return Pair.create(beanA, beanB);
    }
  };

  Server.create = function createServer() {
    const server = Object.create(Server.proto);
    console.log('server created');

    server.setDifficulty(1); // TODO adjustable difficulty

    return server;
  };

  return Server;
})();

// Display /////////////////////////////////////////////////////////////

const Display = (() => {
  const Display = {};

  Display.GRID_PITCH = 24;
  Display.SCALE = Display.GRID_PITCH / 16;

  Display.OFFSETS = {
    [GameLogic.PLAYER]: {
      x: Display.GRID_PITCH * 1,
      y: Display.GRID_PITCH * 1
    },
    [GameLogic.OPPONENT]: {
      x: Display.GRID_PITCH * 13,
      y: Display.GRID_PITCH * 1
    },
  };

  Display.proto = {
    canvases: null,
    assetManager: null,
    clock: null,
    beans: null,

    setContainer: function setContainer($container) {
      const assetMan = this.assetManager;

      const w = Display.GRID_PITCH * 20;
      const h = Display.GRID_PITCH * 20 / Arena.RATIO;
      $container.style.width = w + 'px';
      $container.style.height = h + 'px';

      const canvases = {};
      for (const canvasName of [ 'background', 'beanLayer', 'foreground' ]) {
        const $canvas = document.createElement('canvas');
        $canvas.width = w;
        $canvas.height = h;
        $container.appendChild($canvas);
        canvases[canvasName] = $canvas;
      }

      const bgAsset = assetMan.getAsset('arenaBackground');
      const bgContext = canvases.background.getContext('2d');
      bgContext.drawImage(bgAsset.$img,
        0, 0,
        bgAsset.width, bgAsset.height,
        0, 0,
        w, h);

      const fgAsset = assetMan.getAsset('arenaForeground');
      const fgContext = canvases.foreground.getContext('2d');
      fgContext.drawImage(fgAsset.$img,
        0, 0,
        fgAsset.width, fgAsset.height,
        0, 0,
        w, h);

      // mspf counter
      const beanContext = canvases.beanLayer.getContext('2d');
      beanContext.font = '16px monospace';
      beanContext.fillStyle = 'white';
      beanContext.textBaseline = 'top';

      this.canvases = canvases;
      this.beans = [];
    },

    drawFrame: function drawFrame(time) {
      const $canvas = this.canvases.beanLayer;
      const drawingContext = $canvas.getContext('2d');
      drawingContext.clearRect(0, 0, $canvas.width, $canvas.height);

      for (const bean of this.beans) {
        this.drawBean(bean);
      }
    },

    drawMspf: function drawMspf(mspf) {
      const context = this.canvases.beanLayer.getContext('2d');
      context.fillText('mspf = ' + mspf.toFixed(1),
        Display.GRID_PITCH * 13, Display.GRID_PITCH * 1);
    },

    drawBean: function drawBean(bean) {
      const asset = this.assetManager.getAsset('beanSprites');
      const colorOffset = Bean.COLORS.indexOf(bean.color) * asset.spriteY;

      let stateOffset = NaN;
      switch (bean.visualState) {
        case Bean.STATE_STATIC: {
          stateOffset = bean.bonds * asset.spriteX;
          break;
        }

        case Bean.STATE_LEADING:
        case Bean.STATE_POPPING: {
          stateOffset = Bean.SPRITE_OFFSETS[bean.visualState] * asset.spriteX;
          break;
        }

        case Bean.STATE_INVISIBLE: {
          break;
        }

        default: {
          throw new Error(`unhandled bean state ${bean.visualState}`);
        }
      }

      if (bean.visualState !== Bean.STATE_INVISIBLE) {
        const drawX = Display.OFFSETS[bean.player].x +
          Display.GRID_PITCH * bean.logicalX +
          bean.moveOffsetX + bean.rotationOffsetX;
        const drawY = Display.OFFSETS[bean.player].y +
          Display.GRID_PITCH * bean.logicalY +
          bean.moveOffsetY + bean.rotationOffsetY;
        const drawingContext = this.canvases.beanLayer.getContext('2d');
        drawingContext.drawImage(
          asset.$img,
          stateOffset, colorOffset,
          asset.spriteX, asset.spriteY,
          drawX, drawY,
          Display.GRID_PITCH, Display.GRID_PITCH
        );
      }
    },

    registerBean: function registerBean(bean) {
      if (!this.beans.includes(bean)) this.beans.push(bean);
    },

    unregisterBean: function unregisterBean(bean) {
      const index = this.beans.indexOf(bean);
      if (index < 0) {
        throw new Error('bean not registered: ' + bean);
      }
      this.beans.splice(index, 1);
    },

    startAnimation: function startAnimation(params) {
      return Animation.create(this.clock, params);
    },
  };

  Display.create = function createDisplay() {
    const display = Object.create(Display.proto);
    console.log('display created');
    return display;
  };

  return Display;
})();

// AudioPlayer /////////////////////////////////////////////////////////

const AudioPlayer = (() => {
  const AudioPlayer = {};

  AudioPlayer.proto = {
    assetManager: null,
    audioContext: null,

    playClip: function playClip(clipName) {
      const buffer = this.assetManager.getAsset(clipName).buffer;
      const node = this.audioContext.createBufferSource();
      node.buffer = buffer;
      node.connect(this.audioContext.destination);
      node.start(0);
    },
  };

  AudioPlayer.create = function createAudioPlayer() {
    const audioPlayer = Object.create(AudioPlayer.proto);
    console.log('audioPlayer created');
    return audioPlayer;
  };

  return AudioPlayer;
})();

// Clock ///////////////////////////////////////////////////////////////

const Clock = (() => {
  const Clock = {};

  Clock.ONE_FRAME = 1000 / 60;

  Clock.proto = {
    server: null,

    timeZero: NaN,
    frameCount: NaN,

    periodicTasks: null,
    oneTimeTasks: null,

    addTask: function addTask(params) {
      if (!(params && params.mode && params.callback)) {
        throw new Error('Incorrect call to Clock.addTask ' + params);
      }

      const task = Task.create(params);

      switch (task.mode) {
        case Task.MODE_PER_FRAME: {
          this.periodicTasks.push(task);
          break;
        }

        case Task.MODE_TIMEOUT:
        case Task.MODE_INTERVAL: {
          task.targetFrame = this.frameCount + task.frames;

          // insert then sort
          this.oneTimeTasks.push(task);
          this.oneTimeTasks.sort(
            (taskA, taskB) => taskA.targetFrame - taskB.targetFrame
          );
          break;
        }

        default: {
          throw new Error('Unknown task mode: ', task.mode);
        }
      }

      return task;
    },

    start: function start(callback) {
      let previousTime = -Infinity;

      const mainLoop = (time) => {
        requestAnimationFrame(mainLoop);

        // time provided by requestAnimationFrame has a precision of 1 ms
        // so we add 1 to the time delta for leeway
        if (time - previousTime + 1 >= Clock.ONE_FRAME) {
          previousTime = time;
          this.frameCount = Math.round((time - this.timeZero) / Clock.ONE_FRAME);

          for (const task of Array.from(this.periodicTasks)) {
            if (task.isCancelled) {
              this.periodicTasks.splice(this.periodicTasks.indexOf(task), 1);
            }
            else {
              task.callback(time);
            }
          }

          const oneTimeTasks = this.oneTimeTasks;
          let task;
          while (
            oneTimeTasks[0] &&
            oneTimeTasks[0].targetFrame <= this.frameCount
          ) {
            task = oneTimeTasks.shift();
            if (!task.isCancelled) {
              task.callback(time);
              if (Task.MODE_INTERVAL === task.mode) {
                this.addTask(task);
              }
            }
          }
        }
      };

      requestAnimationFrame((t) => {
        this.timeZero = t;
        this.frameCount = 0;
      });
      if (callback) {
        requestAnimationFrame(callback);
      }
      requestAnimationFrame(mainLoop);
    },
  };

  Clock.create = function createClock() {
    console.log('clock created');
    const clock = Object.create(Clock.proto);

    clock.periodicTasks = [];
    clock.oneTimeTasks = [];

    return clock;
  };

  return Clock;
})();

// Task ////////////////////////////////////////////////////////////////

const Task = (() => {
  const Task = {};

  Task.MODE_TIMEOUT   = Symbol('timeout');
  Task.MODE_INTERVAL  = Symbol('interval');
  Task.MODE_PER_FRAME = Symbol('per frame');

  Task.proto = {
    mode: undefined,
    frames: NaN,
    callback: null,

    targetFrame: NaN,
    isCancelled: false,

    cancel: function cancel() {
      this.isCancelled = true;
    },
  };

  Task.create = function createTask(params) {
    const task = Object.create(Task.proto);

    task.mode = params.mode;
    task.callback = params.callback;
    if (params.frames !== undefined) task.frames = params.frames;

    return task;
  };

  return Task;
})();

// Animation ///////////////////////////////////////////////////////////

const Animation = (() => {
  const Animation = {};

  // hardcoded values so we don’t need to use Math
  Animation.ROTATION_OFFSETS = [ 1, 1, 1, 2, 3, 2, 3, 3 ];
  Animation.ROTATION_FRAMES = Animation.ROTATION_OFFSETS.length;

  Animation.proto = {
    clock: null,
    pendingTasks: null,
    isStopped: false,

    init: function init(params) {
      switch (params.type) {
        case 'loop': {
          const addStepTasks = () => {
            for (const entry of Object.entries(params.steps)) {
              const frame = Number.parseInt(entry[0], 10);
              const callback = entry[1];

              if (!this.isStopped) {
                const task = this.clock.addTask({
                  mode: Task.MODE_TIMEOUT,
                  frames: 1,
                  callback: (time) => {
                     callback(time);
                     this.pendingTasks.delete(task);
                  },
                });
                this.pendingTasks.set(task, true);
              }
            }
            if (!this.isStopped) {
              const task = this.clock.addTask({
                mode: Task.MODE_TIMEOUT,
                frames: params.period,
                callback: (time) => {
                  addStepTasks();
                  this.pendingTasks.delete(task);
                },
              });
              this.pendingTasks.set(task, true);
            }
          };
          addStepTasks();
          break;
        }

        case 'rotation': {
          const slowGrowth = (frame) => Animation.ROTATION_OFFSETS[frame];
          const fastGrowth = (frame) => Animation.ROTATION_OFFSETS[
            Animation.ROTATION_FRAMES - 1 - frame];

          let xGrowth, yGrowth;
          if (
              (params.signX * params.signY > 0 && GameLogic.CW === params.rotDir) ||
              (params.signX * params.signY < 0 && GameLogic.CCW === params.rotDir)
          ) {
            xGrowth = fastGrowth;
            yGrowth = slowGrowth;
          }
          else {
            xGrowth = slowGrowth;
            yGrowth = fastGrowth;
          }

          params.bean.resetRotationOffset();
          params.bean.addRotationOffset(
            -params.signX * Display.GRID_PITCH,
            -params.signY * Display.GRID_PITCH
          );

          let previousTime;
          const addFrameTask = (frame) => {
            this.clock.addTask({
              mode: Task.MODE_TIMEOUT,
              frames: 1,
              callback: (time) => {
                previousTime = time;
                if (!this.isStopped) {
                  if (frame + 1 < Animation.ROTATION_FRAMES) {
                    addFrameTask(frame + 1);
                  }
                  params.bean.addRotationOffset(
                    params.signX * xGrowth(frame) * Display.SCALE,
                    params.signY * yGrowth(frame) * Display.SCALE
                  );
                }
              },
            });
          };
          addFrameTask(0);
          break;
        }

        default: {
          throw new Error(`Unhandled animation type ${param.type}`);
        }
      }
    },

    stop: function stop() {
      this.isStopped = true;
      this.pendingTasks.forEach((_, task) => {
        task.cancel();
      });
      this.pendingTasks.clear();
    },
  };

  Animation.create = function createAnimation(clock, params) {
    const anim = Object.create(Animation.proto);
    anim.clock = clock;
    anim.pendingTasks = new Map();
    anim.init(params);
    return anim;
  };

  return Animation;
})();

// AssetManager ////////////////////////////////////////////////////////

const AssetManager = (() => {
  const AssetManager = {};

  AssetManager.MANIFEST_URL = 'assets.json';
  AssetManager.ALL_ASSETS_LOADED_EVENT = EVENT_NAMESPACE + ':allAssetsLoaded';

  AssetManager.proto = {
    assets: null,
    loadCount: NaN,
    assetCount: NaN,
    errorCount: NaN,
    audioContext: null,

    loadResource: function loadResource(resource) {
      switch (resource.type) {
        case 'image':
          this.loadImage(resource);
          break;

        case 'spritesheet':
          this.loadSpritesheet(resource);
          break;

        case 'audio':
          this.loadAudio(resource);
          break;

        default:
          console.warn('unhandled asset type: %s', resource.type);
          break;
      }
    },

    loadImage: function loadImage(resource) {
      const $img = new Image();

      $img.addEventListener('error', () => {
        console.warn(`image asset "${resource.name}" failed to load`);
        this.errorCount++;
        this.checkCompletion();
      });

      $img.addEventListener('load', () => {
        const asset = this.assets[resource.name] = {
          $img: $img,
          width: resource.width,
          height: resource.height,
        };
        this.loadCount++;
        this.checkCompletion();
      });

      $img.src = resource.url;
    },

    loadSpritesheet: function loadSpritesheet(resource) {
      const $img = new Image();

      $img.addEventListener('error', () => {
        console.warn(`image asset "${resource.name}" failed to load`);
        this.errorCount++;
        this.checkCompletion();
      });

      $img.addEventListener('load', () => {
        this.assets[resource.name] = {
          $img: $img,
          spriteX: resource.spriteX,
          spriteY: resource.spriteY
        };
        this.loadCount++;
        this.checkCompletion();
      });

      $img.src = resource.url;
    },

    loadAudio: function loadAudio(resource) {
      const req = new XMLHttpRequest();
      req.responseType = 'arraybuffer';

      req.onerror = () => {
        console.warn(`audio asset "${resource.name}" failed to load`);
        this.errorCount++;
        this.checkCompletion();
      };

      req.onload = () => {
        this.audioContext.decodeAudioData(req.response, (decodedData) => {
          this.assets[resource.name] = {
            url: resource.url,
            buffer: decodedData,
          };
          this.loadCount++;
          this.checkCompletion();
        }, (error) => {
          console.warn(`could not decode audio data for "${resource.name}"`);
          this.errorCount++;
          this.checkCompletion();
        });
      };

      req.open('GET', resource.url);
      req.send();
    },

    checkCompletion: function checkCompletion() {
      if (this.loadCount + this.errorCount === this.assetCount) {
        console.log(`all assets loaded, ${this.errorCount} error(s)`);
        document.dispatchEvent(
          new CustomEvent(AssetManager.ALL_ASSETS_LOADED_EVENT));
      }
    },

    getAsset: function getAsset(name) {
      if (!(name in this.assets)) {
        throw new Error('asset ' + name + ' not found');
      }
      return this.assets[name];
    },
  };

  AssetManager.create = function createAssetManager() {
    const assetManager = Object.create(AssetManager.proto);
    console.log('assetManager created');

    assetManager.assets = {};
    assetManager.loadCount = 0;
    assetManager.errorCount = 0;

    const req = new XMLHttpRequest();
    req.open('GET', AssetManager.MANIFEST_URL);

    // prevents Firefox from stupidly assuming the file is xml
    req.overrideMimeType('application/json; charset=utf-8');

    req.addEventListener('load', function () {
      let manifest;
      try {
        manifest = JSON.parse(this.responseText);
      } catch (err) {
        console.error('error while parsing asset manifest', err);
      }
      if (!manifest) return;

      assetManager.assetCount = manifest.length;
      for (const resource of manifest) {
        assetManager.loadResource(resource);
      }
    });
    req.send();

    return assetManager;
  };

  return AssetManager;
})();
